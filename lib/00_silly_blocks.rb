def reverser(&proc)
  string = proc.call
  array = string.split(" ")
  array.each_with_index { |word,i| array[i] = word.reverse}
  return array.join(" ")
end

def adder(n=1,&proc)
  num = proc.call
  num + n
end

def repeater(n=1, &proc)
  n.times { proc.call }
end
