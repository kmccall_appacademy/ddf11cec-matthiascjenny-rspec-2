def measure(tms=1,&prc)
  all_times = []
  tms.times do
    start = Time.now
    prc.call
    finish = Time.now
    all_times.push(finish - start)
  end
  (all_times.inject(0, :+))/(all_times.length )
end
